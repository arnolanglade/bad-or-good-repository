### Cache
.PHONY: clean
clean:
	rm -rf var/cache

### CS
.PHONY: cs
cs:
	vendor/bin/php-cs-fixer fix --diff --dry-run

.PHONY: fix-cs
fix-cs:
	vendor/bin/php-cs-fixer fix

### Static analytics
.PHONY: stan
stan:
	vendor/bin/phpstan analyse src -c phpstan.neon -l 0

### Spec
.PHONY: spec
spec:
	vendor/bin/phpspec run

### Behat
.PHONY:behat
behat:
	vendor/bin/behat -p storage_in_memory -f progress
	vendor/bin/behat -p storage_doctrine -f progress

### Test
.PHONY: run-test
run-test: clean cs stan spec behat

### Database
.PHONY:database
database:
	ENV_FILE=database bin/console d:d:c
	ENV_FILE=database bin/console d:s:u --force
