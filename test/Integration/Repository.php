<?php

namespace Test\BouchonnoisCorp\Integration;

use BouchonnoisCorp\Domain\Write;
use Behat\Behat\Context\Context;
use Ramsey\Uuid\Uuid;

final class Repository implements Context
{
    /** @var Write\Repository */
    private $repository;

    /** @var Write\Galinette */
    private $addedGalinette;

    /** @var Write\Galinette */
    private $removedGalinette;

    /** @var bool */
    private $getNonExistingGalinetteError = false;

    /**
     * @param Write\Repository $repository
     */
    public function __construct(Write\Repository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @When a developer adds the galinette to the repository
     */
    public function aDeveloperAddsTheGalinetteToTheRepository()
    {
        $this->addedGalinette = new Write\Galinette(
            new Write\Identifier(Uuid::uuid4()->toString()),
            Write\Birthday::fromString('2013-01-01'),
            new Write\Name('domain\Name'),
            Write\Gender::male()
        );

        $this->repository->add($this->addedGalinette);
    }

    /**
     * @Then a developer gets the same galinette
     *
     * @throws \Exception
     */
    public function aDeveloperGetsTheSameGalinette()
    {
        if ($this->addedGalinette !== $this->repository->get($this->addedGalinette->id())) {
            throw new \Exception('The given galinette is not the same');
        }
    }

    /**
     * @Given a galinette in the repository
     */
    public function aGalinetteInTheRepository()
    {
        $this->removedGalinette = new Write\Galinette(
            new Write\Identifier(Uuid::uuid4()->toString()),
            Write\Birthday::fromString('2013-01-01'),
            new Write\Name('domain\Name'),
            Write\Gender::male()
        );

        $this->repository->add($this->removedGalinette);
    }

    /**
     * @When a developer removes the galinette from the repository
     */
    public function aDeveloperRemovesTheGalinetteFromTheRepository()
    {
        $this->repository->remove($this->removedGalinette->id());
    }

    /**
     * @Then the galinette is removed from the repository
     */
    public function theGalinetteIsRemovedFromTheRepository()
    {
        try {
            $failed = false;
            $this->repository->get($this->removedGalinette->id());
        } catch (Write\Exception\UnknownGalinette $e) {
            $failed = true;
        } finally {
            if (false === $failed) {
                throw new \Exception('The galinette is still is the repository');
            }
        }
    }

    /**
     * @When a developer gets a non existing galinette
     */
    public function aDeveloperGetsANonExistingGalinette()
    {
        try {
            $this->repository->get(new Write\Identifier(Uuid::uuid4()->toString()));
        } catch (Write\Exception\UnknownGalinette $e) {
            $this->getNonExistingGalinetteError = true;
        }
    }

    /**
     * @Then a developer gets an error
     */
    public function aDeveloperGetsAnError()
    {
        if (!$this->getNonExistingGalinetteError) {
            throw new \Exception('The galinette exists but it should not');
        }
    }
}
