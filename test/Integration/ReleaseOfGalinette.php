<?php

namespace Test\BouchonnoisCorp\Integration;

use Behat\Behat\Context\Context;
use BouchonnoisCorp\Domain\Read;
use BouchonnoisCorp\Domain\Write;
use Ramsey\Uuid\Uuid;

class ReleaseOfGalinette implements Context
{
    /** @var Read\ReleaseOfGalinettes */
    private $releaseOfGalinetteQuery;

    /** @var Write\Repository */
    private $galinetteRepository;

    /** @var array */
    private $releasedGalinette;

    /**
     * @param Read\ReleaseOfGalinettes $releaseOfGalinetteQuery
     * @param Write\Repository         $galinetteRepository
     */
    public function __construct(
        Read\ReleaseOfGalinettes $releaseOfGalinetteQuery,
        Write\Repository $galinetteRepository
    ) {
        $this->releaseOfGalinetteQuery = $releaseOfGalinetteQuery;
        $this->galinetteRepository = $galinetteRepository;
    }

    /**
     * @Given a galinette named :name
     */
    public function aGalinette(string $name): void
    {
        $galinette = new Write\Galinette(
            new Write\Identifier(Uuid::uuid4()->toString()),
            Write\Birthday::fromString('2013-01-01'),
            new Write\Name($name),
            Write\Gender::male()
        );

        $this->galinetteRepository->add($galinette);
    }

    /**
     * @When I want to organize a release of galinette
     */
    public function iWantToOrganizeAReleaseOfGalinette()
    {
        $this->releasedGalinette = $this->releaseOfGalinetteQuery->findGalinettes();
    }

    /**
     * @Then Rodrigo and Gabriella are released
     *
     * @throws \Exception
     */
    public function rodrigoAndGabriellaCanBeReleaed()
    {
        $numberOfGalinette = count($this->releasedGalinette);
        if (2 !== $numberOfGalinette) {
            throw new \Exception(
                sprintf('Rodrigo and Gabriella must be released (number of galinette: %d)', $numberOfGalinette)
            );
        }

        foreach ($this->releasedGalinette as $galinette) {
            if (!in_array($galinette->normalize()['name'], ['Rodrigo', 'Gabriella'])) {
                throw new \Exception('Rodrigo and Gabriella must be released');
            }
        }
    }
}
