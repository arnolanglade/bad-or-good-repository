<?php

namespace Specification\BouchonnoisCorp\Infrastructure\Storage\Doctrine;

use BouchonnoisCorp\Domain\Write;
use BouchonnoisCorp\Infrastructure\Storage\Doctrine\Repository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Ramsey\Uuid\Uuid;

class RepositorySpec extends ObjectBehavior
{
    function let(EntityManager $entityManager)
    {
        $this->beConstructedWith($entityManager);
    }

    function it is initializable()
    {
        $this->shouldHaveType(Repository::class);
    }

    function it is a Galinette repository()
    {
        $this->shouldImplement(Write\Repository::class);
    }

    function it gets an galinette($entityManager)
    {
        $galinetteId = Uuid::uuid4()->toString();
        $galinette = new Write\Galinette(
            new Write\Identifier(Uuid::uuid4()->toString()),
            Write\Birthday::fromString('2013-01-01'),
            new Write\Name('domain\Name'),
            Write\Gender::male()
        );

        $entityManager->find(Write\Galinette::class, $galinetteId)->willReturn($galinette);

        $this->get(new Write\Identifier($galinetteId))->shouldReturn($galinette);
    }

    function it throws an exception if an galinette does not exist($entityManager)
    {
        $galinetteId = new Write\Identifier(Uuid::uuid4()->toString());

        $entityManager->find(Write\Galinette::class, $galinetteId)->willReturn(null);

        $this->shouldThrow(Write\Exception\UnknownGalinette::class)->during('get', [$galinetteId]);
    }

    function it adds galinette to repository($entityManager)
    {
        $galinette = new Write\Galinette(
            new Write\Identifier(Uuid::uuid4()->toString()),
            Write\Birthday::fromString('2013-01-01'),
            new Write\Name('domain\Name'),
            Write\Gender::male()
        );

        $entityManager->persist($galinette)->shouldBeCalled();
        $entityManager->flush($galinette)->shouldBeCalled();

        $this->add($galinette)->shouldReturn(null);
    }

    function it throws an exception if something wrong happens when a galinette is added($entityManager)
    {
        $galinette = new Write\Galinette(
            new Write\Identifier(Uuid::uuid4()->toString()),
            Write\Birthday::fromString('2013-01-01'),
            new Write\Name('domain\Name'),
            Write\Gender::male()
        );

        $entityManager->persist($galinette)->willThrow(ORMException::class);

        $this->shouldThrow(\LogicException::class)->during('add', [$galinette]);
    }

    function it removes galinette to repository($entityManager)
    {
        $galinetteIdentifier = new Write\Identifier(Uuid::uuid4()->toString());
        $galinette = new Write\Galinette(
            $galinetteIdentifier,
            Write\Birthday::fromString('2013-01-01'),
            new Write\Name('domain\Name'),
            Write\Gender::male()
        );

        $entityManager->getReference(
            Write\Galinette::class,
            (string) $galinetteIdentifier
        )->willReturn($galinette);

        $entityManager->remove($galinette)->shouldBeCalled();
        $entityManager->flush($galinette)->shouldBeCalled();

        $this->remove($galinetteIdentifier)->shouldReturn(null);
    }

    function it throws an exception if something wrong happens when a galinette is removed($entityManager)
    {
        $galinetteIdentifier = new Write\Identifier(Uuid::uuid4()->toString());
        $galinette = new Write\Galinette(
            $galinetteIdentifier,
            Write\Birthday::fromString('2013-01-01'),
            new Write\Name('domain\Name'),
            Write\Gender::male()
        );

        $entityManager->getReference(
            Write\Galinette::class,
            (string) $galinetteIdentifier
        )->willReturn($galinette);

        $entityManager->remove($galinette)->willThrow(ORMException::class);

        $this->shouldThrow(\LogicException::class)->during('remove', [$galinetteIdentifier]);
    }

    function it creates the next identity()
    {
        $this->nextIdentity()->shouldHaveType(Write\Identifier::class);
    }
}
