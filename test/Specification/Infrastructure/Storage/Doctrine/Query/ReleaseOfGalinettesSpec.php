<?php

namespace Specification\BouchonnoisCorp\Infrastructure\Storage\Doctrine\Query;

use BouchonnoisCorp\Infrastructure\Storage\Doctrine\Query\ReleaseOfGalinettes;
use Doctrine\DBAL\Driver\Connection;
use BouchonnoisCorp\Domain\Read;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ReleaseOfGalinettesSpec extends ObjectBehavior
{
    function let(Connection $connection)
    {
        $this->beConstructedWith($connection);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(ReleaseOfGalinettes::class);
    }

    function it is a query()
    {
        $this->shouldImplement(Read\ReleaseOfGalinettes::class);
    }
}
