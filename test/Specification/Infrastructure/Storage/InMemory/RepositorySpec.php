<?php

namespace Specification\BouchonnoisCorp\Infrastructure\Storage\InMemory;

use BouchonnoisCorp\Domain\Write;
use BouchonnoisCorp\Infrastructure\Storage\InMemory\Repository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Ramsey\Uuid\Uuid;

class RepositorySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Repository::class);
    }

    function it is initializable()
    {
        $this->shouldHaveType(Repository::class);
    }

    function it is a Galinette repository()
    {
        $this->shouldImplement(Write\Repository::class);
    }

    function it gets an galinette()
    {
        $galinetteId = new Write\Identifier(Uuid::uuid4()->toString());
        $galinette = new Write\Galinette(
            $galinetteId,
            Write\Birthday::fromString('2013-01-01'),
            new Write\Name('domain\Name'),
            Write\Gender::male()
        );

        $this->add($galinette);

        $this->get(new Write\Identifier($galinetteId))->shouldReturn($galinette);
    }

    function it adds galinette to repository($entityManager)
    {
        $galinetteId = new Write\Identifier(Uuid::uuid4()->toString());
        $galinette = new Write\Galinette(
            $galinetteId,
            Write\Birthday::fromString('2013-01-01'),
            new Write\Name('domain\Name'),
            Write\Gender::male()
        );

        $this->add($galinette)->shouldReturn(null);
    }

    function it removes galinette to repository($entityManager)
    {
        $galinetteId = new Write\Identifier(Uuid::uuid4()->toString());
        $galinette = new Write\Galinette(
            $galinetteId,
            Write\Birthday::fromString('2013-01-01'),
            new Write\Name('domain\Name'),
            Write\Gender::male()
        );

        $this->add($galinette);
        $this->remove($galinetteId)->shouldReturn(null);

        $this->shouldThrow(Write\Exception\UnknownGalinette::class)->during('get', [$galinetteId]);
    }

    function it creates the next identity()
    {
        $this->nextIdentity()->shouldHaveType(Write\Identifier::class);
    }
}
