<?php

namespace Specification\BouchonnoisCorp\Domain\Write;

use BouchonnoisCorp\Domain\Write\Identifier;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class IdentifierSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('03a368d5-85b2-46cf-a860-ab22101827d8');
    }

    function it is initializable()
    {
        $this->shouldHaveType(Identifier::class);
    }

    function it creates an identifier from a uuid()
    {
        $this->__toString()->shouldReturn('03a368d5-85b2-46cf-a860-ab22101827d8');
    }

    function it throws an exception if it is invalid()
    {
        $this->shouldThrow(\InvalidArgumentException::class)->during('__construct', ['wrong uuid']);
    }
}
