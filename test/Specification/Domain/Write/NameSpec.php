<?php

namespace Specification\BouchonnoisCorp\Domain\Write;

use BouchonnoisCorp\Domain\Write\Name;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class NameSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('name');
    }

    function it is initializable()
    {
        $this->shouldHaveType(Name::class);
    }

    function it is printable()
    {
        $this->__toString()->shouldReturn('name');
    }
}
