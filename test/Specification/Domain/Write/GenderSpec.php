<?php

namespace Specification\BouchonnoisCorp\Domain\Write;

use BouchonnoisCorp\Domain\Write\Gender;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class GenderSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('F');
    }

    function it is initializable()
    {
        $this->shouldHaveType(Gender::class);
    }

    function it is a male()
    {
        $this::male()->shouldBeLike(new Gender('M'));
    }

    function it is a female()
    {
        $this::female()->shouldBeLike(new Gender('F'));
    }

    function it throws an exception if the given gender is invalid()
    {
        $this->shouldThrow(\InvalidArgumentException::class)->during('__construct', ['D']);
    }

    function it is printable()
    {
        $this->__toString()->shouldReturn('F');
    }
}
