<?php

namespace Specification\BouchonnoisCorp\Domain\Write\Exception;

use BouchonnoisCorp\Domain\Write\Exception\UnknownGalinette;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class UnknownGalinetteSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('identifier');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(UnknownGalinette::class);
    }

    function it_is_an_exception()
    {
        $this->shouldHaveType(\Exception::class);
    }
}
