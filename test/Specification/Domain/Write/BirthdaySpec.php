<?php

namespace Specification\BouchonnoisCorp\Domain\Write;

use BouchonnoisCorp\Domain\Write\Birthday;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class BirthdaySpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith(new \DateTimeImmutable('2013-12-12'));
    }

    function it is initializable()
    {
        $this->shouldHaveType(Birthday::class);
    }

    function it constructed from a string()
    {
        $this::fromString('2013-12-12')->shouldBeLike(New Birthday(new \DateTimeImmutable('2013-12-12')));
    }

    function it is printable()
    {
        $this->__toString()->shouldReturn('2013-12-12');
    }
}
