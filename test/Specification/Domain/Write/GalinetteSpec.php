<?php

namespace Specification\BouchonnoisCorp\Domain\Write;

use BouchonnoisCorp\Domain\Write\Birthday;
use BouchonnoisCorp\Domain\Write\Galinette;
use BouchonnoisCorp\Domain\Write\Gender;
use BouchonnoisCorp\Domain\Write\Identifier;
use BouchonnoisCorp\Domain\Write\Name;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Ramsey\Uuid\Uuid;

class GalinetteSpec extends ObjectBehavior
{
    private $identifier;

    function let()
    {
        $this->identifier = new Identifier(Uuid::uuid4()->toString());

        $this->beConstructedWith(
            $this->identifier,
            Birthday::fromString('2013-01-01'),
            new Name('name'),
            Gender::female()
        );
    }

    function it is initializable()
    {
        $this->shouldHaveType(Galinette::class);
    }

    function it borns someday()
    {
        $this::born(new Identifier($this->identifier), Birthday::fromString('2013-01-01'), Gender::male())
            ->shouldBeLike(new Galinette(new Identifier($this->identifier), Birthday::fromString('2013-01-01'), new Name(''), Gender::male()));
    }

    function it is named()
    {
        $this->name(new Name('gabriella'))->shouldReturn(null);
    }

    function it goes to the heaven()
    {
        $this->goToHeaven()->shouldReturn(null);
    }

    function it has a identifier()
    {
        $this->id()->shouldReturn($this->identifier);
    }
}
