<?php

namespace Specification\BouchonnoisCorp\Domain\Read;

use BouchonnoisCorp\Domain\Write\Birthday;
use BouchonnoisCorp\Domain\Write\Identifier;
use BouchonnoisCorp\Domain\Write\Name;
use BouchonnoisCorp\Domain\Read\ReleasedGalinette;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Ramsey\Uuid\Uuid;

class ReleasedGalinetteSpec extends ObjectBehavior
{
    private $identifier;

    function let()
    {
        $this->identifier = Uuid::uuid4()->toString();
        $this->beConstructedWith(
            new Identifier($this->identifier),
            new Name('name'),
            Birthday::fromString('2013-12-12')
        );
    }

    function it is initializable()
    {
        $this->shouldHaveType(ReleasedGalinette::class);
    }

    function it is normalizable()
    {
        $this->normalize()->shouldReturn([
            'id' => $this->identifier,
            'name' => 'name',
            'birthday' => '2013-12-12',
        ]);
    }
}
