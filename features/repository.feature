Feature: Galinette repository
  In order to get a Galinette
  As a developer
  I need to add it into a repository

  @storage-doctrine @storage-in-memory
  Scenario: Add a galinette to the repository
    When a developer adds the galinette to the repository
    Then a developer gets the same galinette

  @storage-doctrine @storage-in-memory
  Scenario: Remove a galinette from the repository
    Given a galinette in the repository
    When a developer removes the galinette from the repository
    Then the galinette is removed from the repository

  @storage-doctrine @storage-in-memory
  Scenario: An exception is thrown when you to get a non existing galinette
    When a developer gets a non existing galinette
    Then a developer gets an error

