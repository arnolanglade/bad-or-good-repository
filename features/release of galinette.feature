Feature: Release of galinette
  In order to be not "broucouille"
  As a hunter
  I need to organize "un laché de galinette"

  @storage-doctrine
  Scenario: Release of galinette
    Given a galinette named Rodrigo
    And a galinette named Gabriella
    When I want to organize a release of galinette
    Then Rodrigo and Gabriella are released
