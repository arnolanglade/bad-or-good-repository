What is the difference between a good and a bad repository? 
===========================================================
[![Build Status](https://scrutinizer-ci.com/g/arnolanglade/galinette-repository/badges/build.png?b=master)](https://scrutinizer-ci.com/g/arnolanglade/galinette-repository/build-status/master)
[![Build Status](https://travis-ci.org/arnolanglade/galinette-repository.svg?branch=master)](https://travis-ci.org/arnolanglade/galinette-repository)

This is the code used for the talk **What is the difference between a good and a bad repository?** at **Forum PHP 2018**.

The slides are [here](https://arnolanglade.gitlab.io/bad-or-good-repository).

Run the tests
-------------

```
 make run-test
```

You should have a look to [FriendOfBehat](https://github.com/FriendsOfBehat) organisation. 
There are lot of interesting Behat extensions!
