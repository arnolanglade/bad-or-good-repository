<?php

$root = sprintf('%s/src', __DIR__);

$finder = PhpCsFixer\Finder::create()
    ->exclude('test/Specification')
    ->in($root)
;

return PhpCsFixer\Config::create()
    ->setRules([
        '@Symfony' => true,
        'array_syntax' => ['syntax' => 'short'],
    ])
    ->setCacheFile(sprintf('%s/var/php_cs.cache', __DIR__))
    ->setFinder($finder)
;
