<?php

declare(strict_types=1);

/*
 * This file is part of the Bouchonnois Corp package
 *
 * (c) Arnaud Langlade
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BouchonnoisCorp\Infrastructure\Storage\Doctrine\Query;

use BouchonnoisCorp\Domain\Read\ReleasedGalinette;
use BouchonnoisCorp\Domain\Read;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\ORM\EntityManagerInterface;

final class ReleaseOfGalinettes implements Read\ReleaseOfGalinettes
{
    /** @var EntityManagerInterface */
    private $connection;

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * {@inheritdoc}
     */
    public function findGalinettes(): array
    {
        $query = <<<SQL
SELECT g.identifier, g.name, g.birthday
FROM galinette as g
SQL;

        $statement = $this->connection->query($query);

        $galinettes = [];
        while ($galinette = $statement->fetch()) {
            $galinettes[] = new ReleasedGalinette(
                $galinette['identifier'],
                $galinette['name'],
                $galinette['birthday']
            );
        }

        return $galinettes;
    }
}
