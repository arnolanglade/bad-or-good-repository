<?php

declare(strict_types=1);

/*
 * This file is part of the Bouchonnois Corp package
 *
 * (c) Arnaud Langlade
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BouchonnoisCorp\Infrastructure\Storage\Doctrine;

use BouchonnoisCorp\Domain\Write\Exception;
use BouchonnoisCorp\Domain\Write;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\ORMInvalidArgumentException;
use Ramsey\Uuid\Uuid;

final class Repository implements Write\Repository
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function get(Write\Identifier $identifier): Write\Galinette
    {
        try {
            $galinette = $this->entityManager->find(Write\Galinette::class, $identifier);
        } catch (ORMInvalidArgumentException | ORMException $e) {
            throw new \LogicException('It is not possible to add this galinette');
        } finally {
            if (null === $galinette) {
                throw new Exception\UnknownGalinette((string) $identifier);
            }

            return $galinette;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function add(Write\Galinette $galinette): void
    {
        try {
            $this->entityManager->persist($galinette);
            $this->entityManager->flush($galinette);
        } catch (ORMInvalidArgumentException | ORMException $e) {
            throw new \LogicException('It is not possible to add this galinette');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function remove(Write\Identifier $identifier): void
    {
        try {
            $galinette = $this->entityManager->getReference(Write\Galinette::class, (string) $identifier);

            $this->entityManager->remove($galinette);
            $this->entityManager->flush($galinette);
        } catch (ORMInvalidArgumentException | ORMException $e) {
            throw new \LogicException('It is not possible to remove this galinette');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function nextIdentity(): Write\Identifier
    {
        try {
            return new Write\Identifier(Uuid::uuid4()->toString());
        } catch (\InvalidArgumentException $e) {
            throw new \LogicException('It is not possible to build the next identity');
        }
    }
}
