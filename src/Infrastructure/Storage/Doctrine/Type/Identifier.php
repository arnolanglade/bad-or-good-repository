<?php

declare(strict_types=1);

/*
 * This file is part of the Bouchonnois Corp package
 *
 * (c) Arnaud Langlade
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BouchonnoisCorp\Infrastructure\Storage\Doctrine\Type;

use BouchonnoisCorp\Domain\Write;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;

final class Identifier extends Type
{
    /**
     * {@inheritdoc}
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return $platform->getGuidTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($uuid, AbstractPlatform $platform): string
    {
        if ($uuid instanceof Write\Identifier) {
            return (string) $uuid;
        }

        throw ConversionException::conversionFailed($uuid, Write\Identifier::class);
    }

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($uuid, AbstractPlatform $platform): Write\Identifier
    {
        try {
            return new Write\Identifier($uuid);
        } catch (\InvalidArgumentException $exception) {
            throw ConversionException::conversionFailed($uuid, Write\Identifier::class);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'galinette_identifier';
    }
}
