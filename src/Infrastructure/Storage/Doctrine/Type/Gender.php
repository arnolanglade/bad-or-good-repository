<?php

declare(strict_types=1);

/*
 * This file is part of the Bouchonnois Corp package
 *
 * (c) Arnaud Langlade
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BouchonnoisCorp\Infrastructure\Storage\Doctrine\Type;

use BouchonnoisCorp\Domain\Write;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;

final class Gender extends Type
{
    /**
     * {@inheritdoc}
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return $platform->getVarcharTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): string
    {
        if ($value instanceof Write\Gender) {
            return (string) $value;
        }

        if (is_string($value)) {
            return $value;
        }

        throw ConversionException::conversionFailed($value, Write\Gender::class);
    }

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): Write\Gender
    {
        try {
            return new Write\Gender($value);
        } catch (\InvalidArgumentException $exception) {
            throw ConversionException::conversionFailed($value, Write\Gender::class);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'galinette_gender';
    }
}
