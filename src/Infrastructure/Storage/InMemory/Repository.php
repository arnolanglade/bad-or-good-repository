<?php

declare(strict_types=1);

/*
 * This file is part of the Bouchonnois Corp package
 *
 * (c) Arnaud Langlade
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BouchonnoisCorp\Infrastructure\Storage\InMemory;

use BouchonnoisCorp\Domain\Write;
use Doctrine\Common\Collections\ArrayCollection;
use Ramsey\Uuid\Uuid;

final class Repository implements Write\Repository
{
    /** @var ArrayCollection */
    private $galinettes;

    public function __construct()
    {
        $this->galinettes = new ArrayCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function get(Write\Identifier $identifier): Write\Galinette
    {
        $galinette = $this->galinettes->filter(function (Write\Galinette $galinette) use ($identifier) {
            return $identifier == $galinette->id();
        });

        if (1 !== count($galinette)) {
            throw new Write\Exception\UnknownGalinette((string) $identifier);
        }

        return $galinette->first();
    }

    /**
     * {@inheritdoc}
     */
    public function add(Write\Galinette $galinette): void
    {
        $this->galinettes->set((string) $galinette->id(), $galinette);
    }

    /**
     * {@inheritdoc}
     */
    public function remove(Write\Identifier $identifier): void
    {
        $this->galinettes->remove((string) $identifier);
    }

    /**
     * {@inheritdoc}
     */
    public function nextIdentity(): Write\Identifier
    {
        try {
            return new Write\Identifier(Uuid::uuid4()->toString());
        } catch (\InvalidArgumentException $e) {
            throw new \LogicException('It is not possible to build the next identity');
        }
    }
}
