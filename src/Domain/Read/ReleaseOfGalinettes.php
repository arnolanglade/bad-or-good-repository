<?php

declare(strict_types=1);

/*
 * This file is part of the Bouchonnois Corp package
 *
 * (c) Arnaud Langlade
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BouchonnoisCorp\Domain\Read;

interface ReleaseOfGalinettes
{
    /**
     * @return ReleasedGalinette[]
     */
    public function findGalinettes(): array;
}
