<?php

declare(strict_types=1);

/*
 * This file is part of the Bouchonnois Corp package
 *
 * (c) Arnaud Langlade
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BouchonnoisCorp\Domain\Read;

final class ReleasedGalinette
{
    /** @var string */
    private $identifier;

    /** @var string */
    private $name;

    /** @var string */
    private $birthday;

    /**
     * @param string $identifier
     * @param string $name
     * @param string $birthday
     */
    public function __construct(
        string $identifier,
        string $name,
        string $birthday
    ) {
        $this->identifier = $identifier;
        $this->name = $name;
        $this->birthday = $birthday;
    }

    /**
     * @return array
     */
    public function normalize(): array
    {
        return [
            'id' => $this->identifier,
            'name' => $this->name,
            'birthday' => $this->birthday,
        ];
    }
}
