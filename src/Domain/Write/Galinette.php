<?php

declare(strict_types=1);

/*
 * This file is part of the Bouchonnois Corp package
 *
 * (c) Arnaud Langlade
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BouchonnoisCorp\Domain\Write;

/*final*/ class Galinette
{
    /** @var Identifier */
    private $identifier;

    /** @var Birthday */
    private $birthday;

    /** @var Name */
    private $name;

    /** @var Gender */
    private $gender;

    /** @var \DateTimeInterface */
    private $diedAt;

    /**
     * @param Identifier $identifier
     * @param Birthday   $birthday
     * @param Name       $name
     * @param Gender     $gender
     */
    public function __construct(Identifier $identifier, Birthday $birthday, Name $name, Gender $gender)
    {
        $this->birthday = $birthday;
        $this->name = $name;
        $this->gender = $gender;
        $this->identifier = $identifier;
    }

    /**
     * @param Identifier $identifier
     * @param Birthday   $birthday
     * @param Gender     $gender
     *
     * @return Galinette
     */
    public static function born(Identifier $identifier, Birthday $birthday, Gender $gender): Galinette
    {
        return new self($identifier, $birthday, new Name(''), $gender);
    }

    /**
     * @param Name $name
     */
    public function name(Name $name): void
    {
        $this->name = $name;
    }

    /**
     * @throws \Exception
     */
    public function goToHeaven(): void
    {
        $this->diedAt = new \DateTimeImmutable('NOW');
    }

    /**
     * @return Identifier
     */
    public function id(): Identifier
    {
        return $this->identifier;
    }
}
