<?php

declare(strict_types=1);

/*
 * This file is part of the Bouchonnois Corp package
 *
 * (c) Arnaud Langlade
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BouchonnoisCorp\Domain\Write;

final class Birthday
{
    /** @var \DateTimeImmutable */
    private $date;

    /**
     * @param \DateTimeImmutable $date
     */
    public function __construct(\DateTimeImmutable $date)
    {
        $this->date = $date;
    }

    /**
     * @param string $date
     *
     * @return Birthday
     *
     * @throws \Exception
     */
    public static function fromString(string $date): Birthday
    {
        return new self(new \DateTimeImmutable($date));
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->date->format('Y-m-d');
    }
}
