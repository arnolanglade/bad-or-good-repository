<?php

declare(strict_types=1);

/*
 * This file is part of the Bouchonnois Corp package
 *
 * (c) Arnaud Langlade
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BouchonnoisCorp\Domain\Write;

final class Identifier
{
    /** @var string */
    private $identifier;

    /**
     * @param string $uuid
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(string $uuid)
    {
        $uuid = str_replace(['urn:', 'uuid:', '{', '}'], '', $uuid);

        if ('00000000-0000-0000-0000-000000000000' != $uuid &&
            !preg_match('/^[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{12}$/', $uuid)) {
            throw new \InvalidArgumentException(sprintf('The given uuid "%s" is invalid', $uuid));
        }

        $this->identifier = $uuid;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->identifier;
    }
}
