<?php

declare(strict_types=1);

/*
 * This file is part of the Bouchonnois Corp package
 *
 * (c) Arnaud Langlade
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BouchonnoisCorp\Domain\Write;

final class Gender
{
    private const FEMALE = 'F';
    private const MALE = 'M';

    /** @var string */
    private $gender;

    /**
     * @param string $gender
     */
    public function __construct(string $gender)
    {
        if (!in_array($gender, [self::MALE, self::FEMALE])) {
            throw new \InvalidArgumentException(
                sprintf('The given %s is not valid', $gender)
            );
        }

        $this->gender = $gender;
    }

    /**
     * @return Gender
     */
    public static function male(): Gender
    {
        return new self(self::MALE);
    }

    /**
     * @return Gender
     */
    public static function female(): Gender
    {
        return new self(self::FEMALE);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->gender;
    }
}
