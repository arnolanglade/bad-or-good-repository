<?php

declare(strict_types=1);

/*
 * This file is part of the Bouchonnois Corp package
 *
 * (c) Arnaud Langlade
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BouchonnoisCorp\Domain\Write\Exception;

use BouchonnoisCorp\Domain\Write\Galinette;

final class UnknownGalinette extends \Exception
{
    /**
     * {@inheritdoc}
     */
    public function __construct($identifier, $code = 0, \Exception $previous = null)
    {
        parent::__construct(
            sprintf('There is no %s with identifier "%s"', Galinette::class, $identifier),
            $code,
            $previous
        );
    }
}
