<?php

declare(strict_types=1);

/*
 * This file is part of the Bouchonnois Corp package
 *
 * (c) Arnaud Langlade
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BouchonnoisCorp\Domain\Write;

interface Repository
{
    /**
     * Find an galinette by its identifier.
     *
     * @param Identifier $identifier
     *
     * @return Galinette
     *
     * @throws Exception\UnknownGalinette | \LogicException
     */
    public function get(Identifier $identifier): Galinette;

    /**
     * Add an galinette.
     *
     * @param Galinette $galinette
     *
     * @throws \LogicException
     */
    public function add(Galinette $galinette): void;

    /**
     * Remove an galinette.
     *
     * @param Identifier $identifier
     *
     * @throws \LogicException
     */
    public function remove(Identifier $identifier): void;

    /**
     * Get the next identity.
     *
     * @return Identifier
     */
    public function nextIdentity(): Identifier;
}
